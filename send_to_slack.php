<?php

$channel = $_POST['channel'];
$title = $_POST['title'];
$image = $_POST['image'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://slack.com/api/chat.postMessage");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


$data = '{
		    "channel": "'.$channel.'",
		    "image_url": "'.$image.'",
		    "text": "'.$title.'"
		 }';

$headers = array();
// $headers[] = 'Authorization: Bearer slackapikey';
$headers[] = 'Content-type: application/json';
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST'); // -X
curl_setopt($ch, CURLOPT_BINARYTRANSFER, TRUE); // --data-binary
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$server_output = curl_exec($ch);
curl_close ($ch);

$result = json_decode($server_output);

print_r($result);

?>