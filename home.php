
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>random meme to slack channel</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/docs/4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

  </head>

  <body>

    <div class="container">		
		<div align="center" style="margin-top:50px">  		
    		<?php 
    		$url = "https://api.imgflip.com/get_memes";
    		
    		$ch = curl_init();
    		$timeout = 5;
    		curl_setopt($ch, CURLOPT_URL, $url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    		$data = curl_exec($ch);
    		curl_close($ch);
    		
    		$result = json_decode($data, true);
        
    		$result_count = sizeof($result['data']['memes']);
    		
    		$random_number = mt_rand(0,$result_count); // because the number of results is not always 100
    		
    		$title = $result['data']['memes'][$random_number]['name'];
    		$image = $result['data']['memes'][$random_number]['url'];
    
    		?>
	
    		<img src="<?php echo $image;?>" class="img-fluid" alt="Responsive image" style="max-height:400px;max-width:100%" id="image">
    		
    		<h3 style="margin-top:40px;margin-bottom:40px" id="meme_title"><?php echo $title;?></h3>
    		
            <div class="input-group input-group-lg" style="width:40%;margin-top:40px;margin-bottom:40px;display:none" id="edit_meme_title">
              <input type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="<?php echo $title;?>" id="new_meme_title">
            </div>

    		<button type="button" class="btn btn-outline-danger" id="cancel" style="display:none"><i class="fas fa-ban"></i></button>  
    		<button type="button" class="btn btn-outline-success" id="save" style="display:none"><i class="fas fa-save"></i></button>  
    		<button type="button" class="btn btn-outline-warning" id="edit"><i class="fas fa-edit" id="edit_icon"></i></button>            
    		<button type="button" class="btn btn-outline-primary" id="refresh"><i class="fas fa-redo"></i></button>
            <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#slack_modal"><i class="fab fa-slack-hash"></i></button>
		</div>
		
    	

        </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/docs/4.1/assets/js/vendor/popper.min.js"></script>
    <script src="http://getbootstrap.com/docs/4.1/dist/js/bootstrap.min.js"></script>
    <script src="http://getbootstrap.com/docs/4.1/assets/js/vendor/holder.min.js"></script>

    
    <!-- Modal -->
    <div class="modal fade" id="slack_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Slack Channel</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" id="channel" placeholder="enter slack channel name" />
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="send_to_slack">Send to Slack</button>
          </div>
        </div>
      </div>
    </div>
    
    
	<script>
	$("#refresh").on('click', function(){
		location.reload();
	});
	</script>

	<script>
	$("#edit").on('click', function(){
		$("#meme_title").hide();
		$("#edit_meme_title").show();
		$("#edit").hide();
		$("#cancel").show();
		$("#save").show();
	});
	</script>
	
	<script>
	$("#cancel").on('click', function(){
		$("#meme_title").show();
		$("#edit_meme_title").hide();
		$("#edit").show();
		$("#cancel").hide();
		$("#save").hide();
	});
	</script>
	
	<script>
	$("#save").on('click', function(){
		var new_title = $("#new_meme_title").val();
		$("#edit_meme_title").hide();
		$("#new_meme_title").val(new_title);
		$("#meme_title").text(new_title);
		$("#meme_title").show();
		$("#edit").show();
		$("#cancel").hide();
		$("#save").hide();
	});
	</script>
	
	<script>
	$("#send_to_slack").on('click', function(e){
		var title = $("#new_meme_title").val();
		var image = "<?php echo $image;?>";
		var channel = $("#channel").val();
	    $.ajax({
			url: "send_to_slack.php",
			data: {title: title, image: image, channel: channel},
			type:'POST',				
			success:function(data){		
			alert("Meme sent to "+channel+"");							
			console.log(data);
			location.reload();
			},
			error:function(data){
			console.log(data);					
			}
			});
			
		e.preventDefault();
		return false;
	});
	</script>

  </body>
</html>
